# Let's make this potion
# Pardon, I meant Elixir ;)

defmodule Algorithm do
  @moduledoc false

  def is_prime?(2) do true end

  def is_prime?(x) when is_integer(x) do
    #checks if x is prime number
   if x>1 do
    if rem(x,2)==0 do false
       else is_prime?(x,3)
       end
   else false
   end
  end

  def is_prime?(x,i) when is_integer(x) and is_integer(i) do
    # checks if x is prime number starting modulo operations from i
    # stops when i > sqrt(x)
    if rem(x,i)== 0 do false
     else if (i*i < x) do is_prime?(x,i+2)
          else true
          end
     end
  end

   def count(l) when is_list(l) do
     #count occurances of every element in list and returns it as a map
     l |> Enum.reduce(%{}, fn(x, acc) -> Map.update(acc, x, 1, &(&1 + 1)) end)
     #reduce(enumerable, acc, fun) invokes fun for each element in the enumerable, passing that element and the accumulator acc as arguments. fun’s return value is stored in acc (which is, at the beginning, an empty map)
     #                                         update(map, key, initial, fun) -> if key exist in map, increment by one, else insert 1
   end

  def weird_algo(a,b) when is_list(a) and is_list(b) do
    # I really had no idea how to name it...
    occ = count(b)
    a |> Enum.filter(& is_prime?(Map.get(occ,&1,0))==false) #filter leaves only those elements for which fun returns a truthy value (not nil, not false)
    #takes every element of a and filters them by checking the condition of occurances
  end
end #end of module


#  c("C:/Users/Karol/IdeaProjects/Elab5/lib/algorithm.ex")


#  https://gist.github.com/anonymous/da5279b9fc1774b12dee78a8ca629768