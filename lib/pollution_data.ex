#Not finished yet!

defmodule PollutionData do
  @moduledoc false
  def importLinesFromCSV (file) do
    File.read! (file) |> String.split()
   end

# DATA,GODZINA,DLUGOSC,SZEROKOSC,WARTOSC
# 03-05-2017,00:15,19.678,49.989,76
   def convert (line) do
     [date, time, longitude, latitude, value] = String.split(line, ",")
     #date = String.split(date, "-") |> Enum.reverse() |> {String.to_integer(elem 1), String.to_integer(elem 2), String.to_integer(elem 3)}
     date = String.split(date, "-") |> Enum.reverse() |> Enum.map(&String.to_integer/1) |> List.to_tuple
     #time = String.split(time, ":") |> {String.to_integer(elem 1), String.to_integer(elem 2), 0}
     time = String.split(time, ":") |> String.to_integer() |> List.to_tuple()
     [date, time, longitude, latitude, value]
   end

   def identifyStations (lines) do
     #is to return list of tuples {longitude, altitude}
     cords = []
     for x <- lines do cords ++ [{elem(x, 3), elem(x, 4)}] end
     Enum.uniq (cords)
   end
end

# font Courier New is the best
#  c("C:/Users/Karol/IdeaProjects/Elab5/lib/pollution_data.ex")

#  PollutionData.convert("04-05-2017,23:35,19.764,50.081,13")
